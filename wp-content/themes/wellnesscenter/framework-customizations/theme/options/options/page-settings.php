<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'page_settings' => array(
        'title' => __('Page Settings', 'wellnesscenter'),
        'type' => 'tab',
        'options' => array(
            'header-page-box' => array(
                'title' => __('Page Settings', 'wellnesscenter'),
                'type' => 'box',
                'options' => array(
                    'page_comment' => array(
                        'label' => __( 'Show Page Comments?', 'wellnesscenter' ),
                        'desc' => '',
                        'type' => 'switch',
                        'right-choice' => array(
                            'value' => 'yes',
                            'label' => __( 'Yes', 'wellnesscenter' )
                        ),
                        'left-choice' => array(
                            'value' => 'no',
                            'label' => __( 'No', 'wellnesscenter' )
                        ),
                        'value' => 'yes',
                    )
                )
            ),
        ),
    ),
);
