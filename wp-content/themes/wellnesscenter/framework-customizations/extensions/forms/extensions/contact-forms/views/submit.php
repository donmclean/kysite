<?php if (!defined('FW')) die('Forbidden');
/**
 * @var string $submit_button_text
 */
?>
<div class="field-submit form-group">
    <div class="row form-actions">
        <div class="col-md-5">
            <button type="submit" class="btn btn-lg btn-success btn-submit"><?php echo esc_html($submit_button_text) ?> <i
                    class="fa fa-caret-right"></i></button>
        </div>
        <div class="col-md-7">
            <p class="form-info"><?php echo __('Send an email. All fields with are required.', 'wellnesscenter'); ?></p>
        </div>
    </div>
</div>
<div class="result"></div>
