<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var string $form_html
 */
?>
<div class="fw-contact-form  contact-2">
    <div class="wrap-forms wrap-contact-forms wow rollIn contact-form" data-wow-offset="10" data-wow-duration="1.55s">
        <?php echo wellnesscenter_return($form_html); ?>
    </div>
</div>