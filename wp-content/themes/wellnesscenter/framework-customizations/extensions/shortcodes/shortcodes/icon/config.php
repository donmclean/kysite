<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Icon Inline', 'wellnesscenter'),
	'description'   => __('Add an Icon', 'wellnesscenter'),
	'tab'           => __('Content Elements', 'wellnesscenter'),
);