<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Pricing Table', 'wellnesscenter' ),
	'description' => __( 'Add a Table', 'wellnesscenter' ),
	'tab'         => __( 'Content Elements', 'wellnesscenter' ),
	'popup_size'  => 'large'
);