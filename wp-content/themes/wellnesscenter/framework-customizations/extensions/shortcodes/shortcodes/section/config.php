<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Layout Elements', 'wellnesscenter'),
		'title'       => __('Section', 'wellnesscenter'),
		'description' => __('Add a Section', 'wellnesscenter'),
		'popup_size'  => 'medium',
		'type'        => 'section' // WARNING: Do not edit this
	)
);