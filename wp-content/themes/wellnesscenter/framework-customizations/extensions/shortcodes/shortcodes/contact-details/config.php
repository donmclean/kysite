<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Contact Details', 'wellnesscenter'),
	'description'   => __('Add Your Contact Details', 'wellnesscenter'),
	'tab'           => __('Content Elements', 'wellnesscenter')
);