<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'         => __('Video', 'wellnesscenter'),
		'description'   => __('Add a Video', 'wellnesscenter'),
		'tab'           => __('Media Elements', 'wellnesscenter'),
	)
);