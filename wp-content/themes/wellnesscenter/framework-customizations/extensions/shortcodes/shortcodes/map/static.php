

<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
$shortcodes_extension_theme = fw()->extensions->get( 'shortcodes' )->get_shortcode('map');
wp_enqueue_style(
	'wellnesscenter-shortcode-map',
	$shortcodes_extension->get_declared_URI('/shortcodes/map/static/css/styles.css')
);

$language = substr( get_locale(), 0, 2 );
wp_enqueue_script(
	'wellnesscenter-google-maps-api-v3',
	'https://maps.googleapis.com/maps/api/js?v=3.15&sensor=false&libraries=places&language=' . $language,
	array(),
	'3.15',
	true
);
wp_enqueue_script(
	'wellnesscenter-shortcode-map-script',
	fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/map/static/js/scripts.js'),
	array('jquery', 'underscore', 'wellnesscenter-google-maps-api-v3'),
	fw()->manifest->get_version(),
	true
);