<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Map', 'wellnesscenter' ),
	'description' => __( 'Add a Map', 'wellnesscenter' ),
	'tab'         => __( 'Content Elements', 'wellnesscenter' )
);