<?php

if (!defined('FW')) {
    die('Forbidden');
}

$map_shortcode = fw_ext('shortcodes')->get_shortcode('map');
$options = array(
    'data_provider' => array(
        'type' => 'multi-picker',
        'label' => false,
        'desc' => false,
        'picker' => array(
            'population_method' => array(
                'label' => __('Population Method', 'wellnesscenter'),
                'desc' => __('Select map population method (Ex: events, custom)', 'wellnesscenter'),
                'type' => 'select',
                'choices' => $map_shortcode->_get_picker_dropdown_choices(),
            )
        ),
        'choices' => $map_shortcode->_get_picker_choices(),
        'show_borders' => true,
    ),
    'map_type' => array(
        'label' => __('Map Type', 'wellnesscenter'),
        'desc' => __('Select map type', 'wellnesscenter'),
        'type' => 'image-picker',
        'value' => '',
        'choices' => array(
            'roadmap' => array(
                'small' => array(
                    'height' => 75,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-roadmap.jpg',
                ),
                'large' => array(
                    'height' => 208,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-roadmap.jpg'
                ),
            ),
            'terrain' => array(
                'small' => array(
                    'height' => 75,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-terrain.jpg',
                ),
                'large' => array(
                    'height' => 208,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-terrain.jpg'
                ),
            ),
            'satellite' => array(
                'small' => array(
                    'height' => 75,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-satellite.jpg',
                ),
                'large' => array(
                    'height' => 208,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-satellite.jpg'
                ),
            ),
            'hybrid' => array(
                'small' => array(
                    'height' => 75,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-hybrid.jpg',
                ),
                'large' => array(
                    'height' => 208,
                    'src' => WELLNESSCENTER_IMAGES . '/image-picker/map-hybrid.jpg'
                ),
            ),
        ),
    ),
    'map_height' => array(
        'label' => __('Map Height', 'wellnesscenter'),
        'desc' => __('Set map height (Ex: 300)', 'wellnesscenter'),
        'type' => 'text'
    )
);
