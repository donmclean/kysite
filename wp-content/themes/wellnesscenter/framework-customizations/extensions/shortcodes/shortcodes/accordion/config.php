<?php

if (!defined('FW')) {
    die('Forbidden');
}

$cfg = array(
    'page_builder' => array(
        'title' => __('Accordion', 'wellnesscenter'),
        'description' => __('Add an Accordion', 'wellnesscenter'),
        'tab' => __('Content Elements', 'wellnesscenter'),
    )
);
