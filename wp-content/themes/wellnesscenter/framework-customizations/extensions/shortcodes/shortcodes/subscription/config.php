<?php

if (!defined('FW'))
    die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Subscription', 'wellnesscenter'),
        'description' => __('Subscription', 'wellnesscenter'),
        'tab' => __('Content Elements', 'wellnesscenter'),
    )
);
