<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Describe Accordion', 'wellnesscenter' ),
		'description' => __( 'Add an Describe Accordion', 'wellnesscenter' ),
		'tab'         => __( 'Content Elements', 'wellnesscenter' ),
	)
);