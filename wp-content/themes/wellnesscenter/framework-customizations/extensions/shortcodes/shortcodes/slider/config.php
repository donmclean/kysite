<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Image Slider', 'wellnesscenter' ),
	'description' => __( 'Add a image slidee show', 'wellnesscenter' ),
	'tab'         => __( 'Content Elements', 'wellnesscenter' ),
);