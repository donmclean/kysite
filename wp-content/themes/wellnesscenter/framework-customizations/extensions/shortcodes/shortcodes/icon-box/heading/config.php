<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Heading', 'wellnesscenter'),
	'description'   => __('Add a Heading', 'wellnesscenter'),
	'tab'           => __('Content Elements', 'wellnesscenter'),
);