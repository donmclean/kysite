<?php

if (!defined('FW'))
    die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Partner', 'wellnesscenter'),
        'description' => __('Our Partner', 'wellnesscenter'),
        'tab' => __('Content Elements', 'wellnesscenter'),
    )
);
