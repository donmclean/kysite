<?php

if (!defined('FW'))
    die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Gellary', 'wellnesscenter'),
        'description' => __('Add Image', 'wellnesscenter'),
        'tab' => __('Content Elements', 'wellnesscenter'),
    )
);
