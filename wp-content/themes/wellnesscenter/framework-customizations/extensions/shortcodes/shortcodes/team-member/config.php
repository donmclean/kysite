<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Team Member', 'wellnesscenter' ),
		'description' => __( 'Add a Team Member', 'wellnesscenter' ),
		'tab'         => __( 'Content Elements', 'wellnesscenter' ),
		'popup_size'  => 'medium'
	)
);