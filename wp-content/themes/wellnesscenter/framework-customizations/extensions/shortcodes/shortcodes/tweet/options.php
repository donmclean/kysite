<?php

if (!defined('FW'))
    die('Forbidden');

$options = array(
    'title' => array(
        'label' => __('Title', 'wellnesscenter'),
        'type' => 'text',
        'value' => 'Latest Tweets',
        'desc' => __('Enter your title here', 'wellnesscenter'),
    ),
);
