<?php

if (!defined('FW'))
    die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Twitter', 'wellnesscenter'),
        'description' => __('Twitter Recent post', 'wellnesscenter'),
        'tab' => __('Content Elements', 'wellnesscenter'),
    )
);
