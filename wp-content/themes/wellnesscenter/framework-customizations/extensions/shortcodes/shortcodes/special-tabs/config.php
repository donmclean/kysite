<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Special Tabs', 'wellnesscenter' ),
		'description' => __( 'Add some Tabs', 'wellnesscenter' ),
		'tab'         => __( 'Content Elements', 'wellnesscenter' ),
	)
);