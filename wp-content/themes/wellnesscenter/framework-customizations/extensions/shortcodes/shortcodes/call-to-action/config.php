<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'         => __('Call To Action', 'wellnesscenter'),
		'description'   => __('Add a Call to Action', 'wellnesscenter'),
		'tab'           => __('Content Elements', 'wellnesscenter'),
		'popup_size'     => 'medium'
	)
);