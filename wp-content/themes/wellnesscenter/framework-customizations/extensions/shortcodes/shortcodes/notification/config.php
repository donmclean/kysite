<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Notification', 'wellnesscenter' ),
	'description' => __( 'Add a Notification Box', 'wellnesscenter' ),
	'tab'         => __( 'Content Elements', 'wellnesscenter' ),
);