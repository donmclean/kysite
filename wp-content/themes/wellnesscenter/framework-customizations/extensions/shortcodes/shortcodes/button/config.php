<?php

if (!defined('FW')) {
    die('Forbidden');
}

$cfg = array(
    'page_builder' => array(
        'title' => __('Button', 'wellnesscenter'),
        'description' => __('Add a Button', 'wellnesscenter'),
        'tab' => __('Content Elements', 'wellnesscenter'),
        'popup_size' => 'medium'
    )
);
