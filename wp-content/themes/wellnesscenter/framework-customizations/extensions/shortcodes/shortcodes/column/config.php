<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Layout Elements', 'wellnesscenter'),
		'title'       => __('Column', 'wellnesscenter'),
		'popup_size'  => 'small',
		'type'        => 'column'
	)
);