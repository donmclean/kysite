<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Feature Box', 'wellnesscenter'),
	'description'   => __('Add a Feature', 'wellnesscenter'),
	'tab'           => __('Content Elements', 'wellnesscenter')
);