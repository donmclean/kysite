<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'         => __('Extra Space', 'wellnesscenter'),
		'description'   => __('Add a Space', 'wellnesscenter'),
		'tab'           => __('Content Elements', 'wellnesscenter'),
	)
);