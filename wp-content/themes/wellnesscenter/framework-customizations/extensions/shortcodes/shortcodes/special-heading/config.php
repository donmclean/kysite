<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Special Heading', 'wellnesscenter'),
	'description'   => __('Add a Special Heading', 'wellnesscenter'),
	'tab'           => __('Content Elements', 'wellnesscenter'),
);