<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'List', 'wellnesscenter' ),
		'description' => __( 'Add a List', 'wellnesscenter' ),
		'tab'         => __( 'Content Elements', 'wellnesscenter' ),
		'popup_size'  => 'small'
	)
);