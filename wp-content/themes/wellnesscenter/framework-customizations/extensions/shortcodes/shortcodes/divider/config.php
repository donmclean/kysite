<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'         => __('Divider', 'wellnesscenter'),
		'description'   => __('Add a Divider', 'wellnesscenter'),
		'tab'           => __('Content Elements', 'wellnesscenter'),
		'popup_size'    => 'medium'
	)
);