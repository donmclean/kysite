<?php 
/**
 * content-none.php
 */
?>

<div class="not-found">
	<h1><?php _e( 'Nothing found!', 'wellnesscenter' ); ?></h1>
</div> <!-- end not-found -->