<?php
/**
 * header.php
 *
 * The header for the theme.
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Favicon, Apple Touch Icons and og icons -->
<?php wellnesscenter_get_header_icons(); ?>

<script type="text/javascript">
    var wellnesscenter_url = '<?php echo esc_url(get_template_directory_uri()); ?>';
    var wellnesscenter_home_url = '<?php echo esc_url(get_home_url('/')); ?>';
</script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >

<!-- Preloader start -->
<div id="page-preloader" class="animated text-center">
    <img src="<?php echo esc_url(wellnesscenter_get_image('loading_img', WELLNESSCENTER_IMAGES . '/preloader-logo.png')); ?>" alt="<?php bloginfo('name'); ?>" class="logo-prelaoder"/>
    <h1 class="title-preloader"><?php bloginfo('name'); ?></h1>
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!-- Preloader end -->


<!-- Navigation Top start -->
<nav class="navbar navbar-default navbar-fixed-top navigation-top" id="navigation-top" >
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Logo start -->
            <a class="navbar-brand nav-logo" href="<?php echo esc_url(get_home_url('/')); ?>">
                <img src="<?php echo esc_url(wellnesscenter_get_image('site_logo', WELLNESSCENTER_IMAGES . '/nav-logo.png')); ?>" alt="<?php bloginfo('name'); ?>"/>
            </a>
            <!-- Logo end -->
        </div>

        <!-- Nav-Links start -->
        <?php
        wp_nav_menu(array(
            'menu' => 'primary',
            'theme_location' => 'primary',
            'depth' => 2,
            'container' => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'bs-navbar-collapse-1',
            'menu_class' => 'nav navbar-nav navbar-right',
            'fallback_cb' => 'wellnesscenter_navwalker::fallback',
            'walker' => new wellnesscenter_navwalker())
        );
        ?>
        <!-- Nav-Links end -->
    </div>
</nav>
<!-- Navigation Top end -->
